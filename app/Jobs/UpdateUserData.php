<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateUserData extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    /**
     * @var User
     */
    protected $user;
    protected $twino_user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, $twino_user)
    {
        $this->user = $user;
        $this->twino_user = $twino_user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(UserRepository $user_repo)
    {
        \DB::reconnect();
        if ($this->user->exists() && \Cache::has('userdata_job_'.$this->user->id)) {
            $user_repo->updateData($this->user, $this->twino_user);
        }
        \Cache::forget('userdata_job_'.$this->user->id);
    }
}
