<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use File;
class OneskySync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'onesky:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronise les trauductions avec OneSky';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $locales = \Config::get('translatable.locales');
        if (is_array($locales)) {
            $onesky_client = $this->laravel->make('onesky');
            $base_locale   = \Config::get('onesky.base_locale');
            $repo          = \App::make('App\Repositories\CategoryRepository');
            $a_push        = [];
            $categories    = $repo->all();
            $categories->load('site', 'translations');
            foreach ($categories as $cat) {
                if ($cat->site) {
                    $site_name = $cat->site->name;
                } else {
                    $site_name = 'all';
                }
                $trans = $cat->translate($base_locale);
                if ($trans != null) {
                    $a_push[$site_name . '-' . $cat->id] = $trans->name;
                }
            }
            $dir = storage_path('tmp/translation');
            if (!is_dir($dir)) {
                File::makeDirectory($dir, 0755, true);
            }
            $categories_file = $dir . '/categories.php';
            file_put_contents($categories_file, '<?php return ' . var_export($a_push, true) . ';');
            $onesky_client->files(
                'upload',
                [
                    'project_id'  => \Config::get('onesky.default_project_id'),
                    'file'        => $categories_file,
                    'file_format' => 'PHP',
                    'locale'      => $base_locale,
                    'is_keeping_all_strings' => false
                ]
            );
            unlink($categories_file);
            $this->info('Categories push ok');

            foreach ($locales as $locale) {
                if ($locale !== $base_locale) {
                    $s_tab = $onesky_client->translations(
                        'export',
                        [
                            'project_id'       => \Config::get('onesky.default_project_id'),
                            'locale'           => $locale,
                            'source_file_name' => 'categories.php',
                        ]
                    );
                    if (!empty($s_tab)) {
                        $this->info('-------------------------------------');
                        $this->info(print_r($s_tab, true));
                        $this->info('-------------------------------------');
                        if ($this->confirm('Le code php est valide ? [y|N]')) {
                            $tab = eval(str_replace('<?php', '', $s_tab));
                            foreach ($tab as $key => $value) {
                                $ex = explode('-', $key);
                                if (count($ex) == 2) {
                                    $cat = $categories->find((int)$ex[1]);
                                    if (!empty($cat)) {
                                        $cat->translateOrNew($locale)->name = $value;
                                        $cat->save();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $this->info('Categories pull ok');
            $this->call('onesky:push', []);
            $this->call('onesky:pull', ['--lang' => implode(',', $locales)]);


        } else {
            $this->error('Invalid locales');
        }

    }
}
