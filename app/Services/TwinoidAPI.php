<?php
namespace App\Services;

class TwinoidAPI extends \TwinoidAPI
{
    const USER_FIELDS = 'name,picture,locale,sites.fields( points,npoints,site.fields(id,name,lang,infos.fields(cover),status), stats.fields(id,name,score,icon,description), achievements.fields(id,name,stat,score,points,npoints,data,date))';
    public function getUser($userId, $fields = self::USER_FIELDS)
    {
        return parent::getUser($userId, $fields);
    }
    public function getMe($fields = self::USER_FIELDS){
        return parent::getMe($fields);
    }
    public function getContacts(){
        return parent::getMe('contacts.fields(user.fields(name,picture))');
    }
}