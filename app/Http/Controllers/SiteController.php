<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Repositories\SiteRepository;
use Auth;

class SiteController extends Controller
{
    /**
     * @var \App\Repositories\SiteRepository
     */
    private $sites;

    public function __construct(SiteRepository $sites)
    {
        $this->sites = $sites;
    }

    public function index()
    {
        $user_id = 0;
        if (Auth::check()) {
            $user_id = Auth::user()->id;
        }
        return view('site.list', ['sites' => $this->sites->getAll($user_id)]);
    }

    public function showStats($id)
    {
        $id      = (int)$id;
        $user_id = 0;
        if (Auth::check()) {
            $user_id = Auth::user()->id;
        }
        $site = $this->sites->getById($id, $user_id);
//
//            $user      = Auth::user();
//            $user_site = $user->sites()->with('stats')->findOrFail($id);
//            foreach ($user_site->stats as $stat) {
////                $site->stats->find($stat->id)->score = $stat->score;
////                Debugbar::info($site->stats->find($stat->id)->score);
//                Debugbar::info($stat);
//            }
//        }
        return view('site.single', ['site' => $site]);
    }
}
