$( document ).ready( function() {
    var $grid = $( '.site-stats' ), sizer;
    if ( $grid.length ) {
        $sizer = $( '<div class="col-xs-1 shuffle__sizer"> </div>' ).appendTo( $grid );
        $grid.shuffle( {
            itemSelector: '.box-stats',
            sizer: $sizer
        } );
        $( '.category-filter' ).on( 'click', function( e ) {
            var $this = $( this );
            e.preventDefault();
            $( '.category-filter.child' ).parent().removeClass( 'active' );
            if ( $this.hasClass( 'child' ) ) {
                $( this ).parent().addClass( 'active' );
            } else if ( $this.parent().not( '.treeview' ).length > 0 ) {
                $this.parent().parent().find( '.active' ).removeClass( 'active' );
                $this.parent().addClass( 'active' );
                $( '.treeview-menu.menu-open' ).slideUp( $.AdminLTE.options.animationSpeed, function() {
                    $( this ).removeClass( 'menu-open' );
                } );
            }
            $grid.shuffle( 'shuffle', $this.data( 'group' ) );
            $( '.main-search' ).trigger( 'change' );
        } );
        $( '.sidebar-form' ).on( 'submit', function( e ) {
            e.preventDefault();
        } );

        $( '.main-search' ).on( 'keyup change', function() {
            var val = this.value.toLowerCase();
            $grid.shuffle( 'shuffle', function( $el, shuffle ) {
                var text = $.trim( $el.find( '.box-title, .achievement-title' ).text() ).toLowerCase();

                // Only search elements in the current group
                if ( shuffle.group !== 'all' && $.inArray( shuffle.group, $el.data( 'groups' ) ) === -1 ) {
                    return false;
                }
                return text.indexOf( val ) !== -1;
            } );
        } );
        $( '.add-cat' ).on( 'click', function( e ) {
            var dialog, $this = $( this );
            e.preventDefault();
            $loader = $( 'body' ).overlaySpinner();
            $loader.open();
            $.get( $this.data( 'href' ), [], function( html ) {
                $loader.close();
                dialog = bootbox.dialog( {
                    buttons: {
                        'save': {
                            className: 'button-save btn-primary',
                            label: Lang.get( 'messages.enregistrer' ),
                            callback: function( e ) {
                                var $button = dialog.find( '.button-save' );
                                var $form = $( '.edit-category-form' );
                                $button.prop( 'disabled', true );
                                $.post( $form.attr( 'action' ), $form.serialize(), function( updating ) {
                                    if ( updating == true ) {
                                        document.location.reload();
                                    } else {
                                        $button.prop( 'disabled', false );
                                        bootbox.alert( {
                                            className: 'modal-error',
                                            message: 'Une erreur s\'est produite'
                                        } );
                                    }
                                } )
                                    .fail( function() {
                                        bootbox.alert( 'La catégorie doit avoir un nom.' );
                                        $button.prop( 'disabled', false );
                                    } );
                                return false;
                            }
                        }
                    },
                    className: 'modal-default',
                    title: Lang.get( 'messages.ajouter-categorie' ),
                    message: html
                } );
            } ).fail( function() {
                $loader.close();
                bootbox.alert( {
                    message: 'Une erreur s\'est produite',
                    className: 'modal-danger',
                    buttons: {
                        ok: {
                            label: 'Ok',
                            className: 'btn-outline',
                            callback: function() {
                                return false;
                            }
                        }
                    }
                } );
            } );
        } );
        $( '.edit-stat-cat' ).on( 'click', function( e ) {
            var dialog,
                $this = $( this );
            e.preventDefault();
            $loader = $( 'body' ).overlaySpinner();
            $loader.open();
            $.get( $this.data( 'href' ), [], function( html ) {
                $loader.close();
                dialog = bootbox.dialog( {
                    buttons: {
                        'save': {
                            className: 'button-save btn-primary',
                            label: Lang.get( 'messages.enregistrer' ),
                            callback: function( e ) {
                                var $button = dialog.find( '.button-save' );
                                var $form = $( '.edit-cat-stat-form' );
                                $button.prop( 'disabled', true );
                                $.post( $form.attr( 'action' ), $form.serialize(), function( cat ) {
                                    $this.find( 'span' ).text( cat.name );
                                    dialog.modal( 'hide' );
                                } ).fail( function( jqXHR ) {
                                    var messages = [];
                                    if ( jqXHR.status === 422 ) {
                                        $.each( jqXHR.responseJSON, function( k, v ) {
                                            messages.push( v );
                                        } );
                                        bootbox.dialog( {
                                            className: 'modal-danger',
                                            title: 'Error',
                                            message: messages.join( '<br/>' )
                                        } );
                                    }
                                } ).always( function() {
                                    $button.prop( 'disabled', false );
                                } );
                                return false;
                            }
                        }
                    },
                    className: 'modal-default',
                    title: Lang.get( 'messages.editer-categorie' ),
                    message: html
                } );
            } ).fail( function() {
                $loader.close();
                bootbox.alert( {
                    message: 'Une erreur s\'est produite',
                    className: 'modal-danger',
                    buttons: {
                        ok: {
                            label: 'Ok',
                            className: 'btn-outline',
                            callback: function() {
                                return false;
                            }
                        }
                    }
                } );
            } );
        } );
        $( '.btn-edit-category' ).on( 'click', function( e ) {
            var dialog, $this = $( this ), $form;
            $loader = $( 'body' ).overlaySpinner();
            $loader.open();
            e.stopPropagation();
            $.get( $this.data( 'href' ), [], function( html ) {
                $loader.close();
                dialog = bootbox.dialog( {
                    buttons: {
                        'save': {
                            className: 'button-save btn-primary',
                            label: Lang.get( 'messages.enregistrer' ),
                            callback: function( e ) {
                                var $button = dialog.find( '.button-save' );
                                var $form = $( '.edit-category-form' );
                                $button.prop( 'disabled', true );
                                $.ajax( $form.attr( 'action' ), {
                                    data: $form.serialize(),
                                    method: 'PUT',
                                    success: function( updating ) {
                                        if ( updating == true ) {
                                            document.location.reload();
                                        } else {
                                            $button.prop( 'disabled', false );
                                            bootbox.alert( {
                                                className: 'modal-error',
                                                message: 'Une erreur s\'est produite'
                                            } );
                                        }
                                    }
                                } )
                                    .fail( function() {
                                        bootbox.alert( 'La catégorie doit avoir un nom.' );
                                        $button.prop( 'disabled', false );
                                    } );
                                return false;
                            }
                        }
                    },
                    className: 'modal-default',
                    title: Lang.get( 'messages.editer-categorie' ),
                    message: html
                } );
            } ).fail( function() {
                $loader.close();
                bootbox.alert( {
                    message: 'Une erreur s\'est produite',
                    className: 'modal-danger',
                    buttons: {
                        ok: {
                            label: 'Ok',
                            className: 'btn-outline',
                            callback: function() {
                                return false;
                            }
                        }
                    }
                } );
            } );

        } );
    }
} );
