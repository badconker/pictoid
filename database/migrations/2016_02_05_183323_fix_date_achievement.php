<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixDateAchievement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('achievement_user', function($table)
        {
            $table->dateTime('date');
        });
        Schema::table('achievements', function($table)
        {
            $table->dropColumn('date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('achievements', function($table)
        {
            $table->dateTime('date');
        });
        Schema::table('achievement_user', function($table)
        {
            $table->dropColumn('date');
        });
    }
}
