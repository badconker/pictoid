<?php

namespace App\Http\Controllers;

use App\Jobs\UpdateUserData;
use App\Repositories\UserRepository;
use Auth;
use Config;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Redirect;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /**
     * @var \App\Repositories\UserRepository
     */
    private $users;
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */
    protected $authTwinoid;
    use AuthenticatesUsers;

    /**
     * Create a new authentication controller instance.
     *
     */
    public function __construct(UserRepository $users)
    {
//        $this->middleware('guest', ['except' => 'getLogout']);
        $config            = Config::get('services.twinoid');
        $this->authTwinoid = new \AuthTwinoid(
            $config['client_id'],
            $config['client_secret'],
            $config['redirect_uri'],
            $config['scope']
        );
        $this->users       = $users;
    }

    public function redirectToProvider(Request $request)
    {
        $request->session()->put('redirect_url', \URL::previous());
        return Redirect::to($this->authTwinoid->getAuthorizationUri());
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @param $request
     * @return Response
     */
    public function handleProviderCallback(Request $request)
    {
        if ($request->has('code')) {
            $this->authTwinoid->initialize($request->input('code'));
            $request->session()->put('twinoid_token', $this->authTwinoid->getToken());

            $twinoidAPI = new \App\Services\TwinoidAPI($this->authTwinoid->getToken());
            $user       = $twinoidAPI->getMe();

            $authUser = $this->users->updateOrCreate($user);
            $job      = (new UpdateUserData($authUser, $user))/*->delay(20)*/
            ;
            $this->dispatch($job);
            \Cache::put('userdata_job_' . $authUser->id, true, 5);
            Auth::login($authUser);
            $redirect_url = $request->session()->get('redirect_url');
            if ($redirect_url != null) {
                $request->session()->forget('redirect_url');
                return Redirect::to($redirect_url);
            }
            return Redirect::route('home');
        }
    }
}
