<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $e
     * @return void
     */
    public function report(Exception $e)
    {
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof ModelNotFoundException) {
            $e = new NotFoundHttpException($e->getMessage(), $e);
        }

        if (!($e instanceof ValidationException)) {
            if (config('app.debug')) {
                if ($request->isXmlHttpRequest()) {
                    return Response::json(
                        [
                            'error' => [
                                'exception' => class_basename($e),
                                'message'   => class_basename($e) . ' in ' .
                                               basename(
                                                   $e->getFile()
                                               ) . ' line ' . $e->getLine() . ': ' . $e->getMessage(),
                            ]
                        ],
                        500
                    );
                }
                return $this->renderExceptionWithWhoops($e);
            } else {
                if ($request->isXmlHttpRequest()) {
                    return Response::json(
                        [
                            'error' => [
                                'exception' => class_basename($e),
                                'message'   => $e->getMessage(),
                            ]
                        ],
                        500
                    );
                }
            }
        }
        if ($e instanceof \TwinoidAPIException && $e->getMessage()) {
            dd('perte connexion twinoid');
        }
        return parent::render($request, $e);
    }

    protected function renderExceptionWithWhoops(Exception $e)
    {
        $whoops = new \Whoops\Run;
        $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler());

        return new \Illuminate\Http\Response(
            $whoops->handleException($e),
            $e->getStatusCode(),
            $e->getHeaders()
        );
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest('login');
    }
}
