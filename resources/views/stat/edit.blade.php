<div class="row edit-cat-stat-modal">
    <div class="col-md-12">
        {!! Form::open(array('route' => ['stat.update', $stat->id], 'method' => 'put', 'class' => 'edit-cat-stat-form')) !!}
        {!! Form::select('category', $categories->pluck('name','id'), $stat->category->id, ['class'=> 'form-control'])!!}
        <div class="alert alert-info" style="margin-top: 20px;">
            <h4><i class="icon fa fa-info"></i>Attention</h4>
            Les modifications ne seront totalement prises en compte qu'après le rechargement de la page
        </div>
        {!! Form::close() !!}
    </div>
</div>