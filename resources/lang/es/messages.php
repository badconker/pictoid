<?php

return [
    'tous_les_jeux' => 'todos los juegos',
    'pictos' => 'Iconos',
    'recherche' => 'Investigación',
    'toutes-categories' => 'todas',
    'ajouter' => 'añadir',
    'ajouter-categorie' => 'añadir una categoría',
    'editer-categorie' => 'Editar una categoría',
    'nom-categorie' => 'Nombre',
    'enregistrer' => 'Registrar',
    'maj-en-cours' => 'Actualización corriente, vuelva dentro de algunos instantes.',
];
