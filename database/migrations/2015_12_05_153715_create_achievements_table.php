<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAchievementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('achievements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('twino_id',100)->unique();
            $table->enum('type', ['default', 'title', 'icon', 'image']);
            $table->integer('stat_id')->unsigned()->nullable()->index();
            $table->foreign('stat_id')->references('id')->on('stats')->onDelete('cascade');
            $table->integer('site_id')->unsigned()->index();
            $table->foreign('site_id')->references('id')->on('sites')->onDelete('cascade');
            $table->integer('score');
            $table->integer('points');
            $table->decimal('npoints', 16, 13);
            $table->dateTime('date');
            $table->string('url')->nullable();
            $table->boolean('prefix')->nullable();
            $table->boolean('suffix')->nullable();
            $table->timestamps();
        });
        Schema::create('achievement_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('achievement_id')->unsigned()->index();
            $table->string('name');
            $table->string('title')->nullable();
            $table->string('locale',10)->index();

            $table->unique(['achievement_id', 'locale']);
            $table->foreign('achievement_id')->references('id')->on('achievements')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('achievement_translations');
        Schema::drop('achievements');
    }
}
