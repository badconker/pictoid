<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>Pictoid | @yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ app()->isLocal() ? asset('assets/css/pictoid.css') : elixir('assets/css/pictoid.css')}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini" data-locale="{{App::getLocale()}}">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a href="{!!route('home')!!}" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><strong>P</strong></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">{!! Html::image('assets/img/logo.png', 'Pictoid') !!}</span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    @if(Auth::check())
                        @if(Cache::has('userdata_job_'.Auth::user()->id))
                            <li class="load-job load-job-data-user" data-route="user-job-data-check">
                                <a href="#">
                                    <i class="fa fa-spin fa-refresh"></i>
                                    <span class="label label-success">
                                        <i class="fa fa-check"></i>
                                    </span>
                                </a>
                            </li>
                        @endif
                        <li class="dropdown user user-menu">
                            <!-- Menu Toggle Button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <!-- The user image in the navbar-->
                                <img src="{{Auth::user()->avatar}}" class="user-image" alt="User Image">
                                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                <span class="hidden-xs">{{Auth::user()->name}}</span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- The user image in the menu -->
                                <li class="user-header">
                                    <img src="{{Auth::user()->avatar}}" class="img-circle" alt="User Image">
                                    <p>
                                        {{Auth::user()->name}}
                                    </p>
                                </li>
                                <!-- Menu Body -->
                                <li class="user-body">
                                    {{--<div class="col-xs-4 text-center">--}}
                                    {{--<a href="#">Followers</a>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-xs-4 text-center">--}}
                                    {{--<a href="#">Sales</a>--}}
                                    {{--</div>--}}
                                    <?php /*<div class="col-xs-4 text-center">
                                        {!! link_to_route('contacts',ucfirst(trans('messages.contacts'))) !!}
                                    </div>*/?>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    {{--<div class="pull-left">--}}
                                    {{--<a href="#" class="btn btn-default btn-flat">Profile</a>--}}
                                    {{--</div>--}}
                                    <div class="pull-right">
                                        <a href="{{route('logout')}}"
                                           class="btn btn-default btn-flat">{{ucfirst(trans('messages.deconnexion'))}}</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    @else
                        <li>
                            <a href="{{route('login')}}"><i
                                        class="fa fa-sign-in fa-fw"></i>{{ucfirst(trans('messages.connexion'))}}</a>
                        </li>
                    @endif
                </ul>
            </div>
        </nav>
    </header>
@include('components.sidebar-main')
<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?php echo e($__env->yieldContent('title')); ?>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">

            @yield('content')

        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
    <?php
    $locales = Config::get('laravellocalization.supportedLocales');
    ?>

    <!-- To the right -->
        <div class="pull-right hidden-xs">
            <ul class="language-switcher">
                @foreach($locales as $code => $locale)
                    <li>
                        <a href="{{LaravelLocalization::getLocalizedURL($code)}}"><i {!! Html::attributes(['class'=>'flag flag-'.e($code)]) !!}></i></a>
                    </li>
                @endforeach
            </ul>
        </div>
        <!-- Default to the left -->
        {{trans('messages.non_officiel')}}

    </footer>

</div><!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- AdminLTE App -->
<script src="{{ app()->isLocal() ? asset('assets/js/pictoid.js') : elixir('assets/js/pictoid.js')}}"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->
</body>
</html>
