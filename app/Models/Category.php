<?php

namespace App\Models;

use App\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use Translatable;
    public $translatedAttributes = ['name'];

    public function site()
    {
        return $this->belongsTo('App\Models\Site');
    }

    public function getSlugAttribute()
    {
        return str_slug($this->name);
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Category', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Category', 'parent_id');
    }
}
