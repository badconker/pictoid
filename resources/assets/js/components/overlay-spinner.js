(function( $ ) {
    $.OverlaySpinner = function( el, options ) {
        // To avoid scope issues, use 'base' instead of 'this'
        // to reference this class from internal events and functions.
        var base = this;

        // Access to jQuery and DOM versions of element
        base.$el = $( el );
        base.el = el;

        // Add a reverse reference to the DOM object
        base.$el.data( 'OverlaySpinner', base );

        base.init = function() {
            base.options = $.extend( {}, $.OverlaySpinner.defaultOptions, options );
            base.spinner = $( '<div class="overlay-spinner closed"><div class="icone la-ball-clip-rotate-pulse la-2x">' +
                '<div></div>' +
                '<div></div>' +
                '</div></div>' ).appendTo( base.$el );
        };

        base.open = function( paramaters ) {
            base.spinner.removeClass( 'closed' );
        };
        base.close = function( paramaters ) {
            base.spinner.addClass( 'closed' );
        };

        // Run initializer
        base.init();
        return base;
    };

    $.OverlaySpinner.defaultOptions = {};

    $.fn.overlaySpinner_each = function( options ) {
        return this.each( function() {
            ( new $.OverlaySpinner( this, options ) );
        } );
    };
    $.fn.overlaySpinner = function( options ) {
        if ( ! this.data( 'OverlaySpinner' ) ) {
            this.each( function() {
                ( new $.OverlaySpinner( this, options ) );
            } );
        }
        return this.data( 'OverlaySpinner' );
    };
})( jQuery );
