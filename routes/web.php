<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::group(
    [
        'prefix'     => LaravelLocalization::setLocale(),
        'middleware' => ['localeSessionRedirect', 'localizationRedirect']
    ],
    function () {
        Route::get('/', ['as' => 'home', 'uses' => 'SiteController@index']);

        // Authentification
        /*Route::get(
            'auth/debug',
            [
                'as'   => 'login_debug',
                'uses' => 'AuthController@loginDebug'
            ]
        );*/

        // User
        Route::get(
            'user/profile',
            [
                'as'   => 'profile',
                'uses' => 'UserController@showProfile'
            ]
        );
        /*Route::get(
            'user/contacts',
            [
                'as'   => 'contacts',
                'uses' => 'UserController@showContacts'
            ]
        );
        Route::post(
            'user/contact/id',
            [
                'as'   => 'contacts-id',
                'uses' => 'UserController@ajaxGetContactsId'
            ]
        );
        Route::post(
            'user/contact',
            [
                'as'   => 'add-contact-twino',
                'uses' => 'UserController@ajaxAddContactUser'
            ]
        );*/

        //Site(s) / Game(s)
        Route::get(
            'game/{id}',
            [
                'as'   => 'show-stats',
                'uses' => 'SiteController@showStats'
            ]
        );
    }
);

Route::post(
    'user/updating',
    [
        'as'   => 'user-job-data-check',
        'uses' => 'UserController@ajaxCheckDataJob'
    ]
);
Route::get(
    'auth/login',
    [
        'as'   => 'login',
        'uses' => 'AuthController@redirectToProvider'
    ]
);
Route::get(
    'auth/logout',
    [
        'as'   => 'logout',
        'uses' => 'AuthController@logout'
    ]
);
Route::get(
    'auth/callback',
    [
        'as'   => 'twinoid-callback',
        'uses' => 'AuthController@handleProviderCallback'
    ]
);
Route::group(
    [
        'middleware' => ['permission:edit-category','ajax']
    ],
    function () {
        Route::resource(
            'category',
            'CategoryController',
            ['only' => ['create', 'store', 'edit', 'update']]
        );
        Route::resource(
            'stat',
            'StatController',
            ['only' => ['edit', 'update']]
        );
    }
);
