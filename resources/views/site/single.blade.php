@extends('layouts.app')

@section('title', $site->name)

@section('content')
    <div class="row site-stats">
        @foreach($site->stats as $stat)
            <?php $score = null;
            $cats = [str_slug($stat->category->name).'-'.$stat->category->id];
            if ($stat->category->parent) {
                $cats[] = str_slug($stat->category->parent->name).'-'.$stat->category->parent->id;
            }
            ?>
            @if(!$stat->users->isEmpty())
                <?php $score = $stat->users->first()->pivot->score; ?>
            @endif
            <div class="col-xs-12 col-sm-6 col-md-4 box-stats"
                 data-groups='{{json_encode($cats)}}'>
                <div class="box box-solid box-primary">
                    <div class="box-header">
                        @permission('edit-category')
                        <span class="category">
                            <a href="#" data-href="{{ route('stat.edit', ['id' => $stat->id]) }}" class="edit-stat-cat"
                               data-stat_id="{{$stat->id}}">
                                <span>{{$stat->category->name}}</span>
                                <i class="fa fa-fw fa-pencil"></i>
                            </a>
                        </span>
                        @endpermission
                        <div class="stat">
                            <div class="stat-icon-container">
                                @if(!empty($stat->icon)){!! Html::image($stat->icon, $stat->name,['class'=>'stat-icon']) !!}@endif
                            </div>
                            <div class="box-title-container">
                                <h3 class="box-title{{strlen($stat->name) > 25 ? ' small' :'' }}">{{$stat->name}}</h3>
                            </div>
                            <div class="score-container">
                                <span class="label bg-aqua">{{$score}}</span>
                            </div>
                        </div>
                        @if($stat->description)<h4 class="desc">{{$stat->description}}</h4>@endif
                    </div>
                    <div class="box-body">
                        <ul class="list-unstyled">
                            @foreach($stat->achievements as $achievement)
                                <li {!! !$achievement->users->isEmpty() ? 'class="user-has"' : '' !!}>
                                    <span class="score">x{{$achievement->score}}</span>
                                    <span class="achievement-title">@include('components.achievement')</span>
                                    <span class="npoints">+{{$achievement->present()->points}}</span>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection