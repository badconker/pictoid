<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

/**
 * App\Models\Site
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Stat[] $stats
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Achievement[] $achievements
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 */
class Site extends Model
{
    public $incrementing = false;
    protected $guarded = array('created_at', 'updated_at');

    public function stats()
    {
        return $this->hasMany('App\Models\Stat')->with('translations', 'category');
    }

    public function achievements()
    {
        return $this->hasMany('App\Models\Achievement')->with('translations');
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User')->withPivot('npoints');
    }

    public function categories()
    {
        return $this->hasMany('App\Models\Category');
    }
    public function getCategoriesStatsAttribute()
    {
        return $this->stats->pluck('category')->unique()->sortBy('name');
    }

    public function getProgressionGeneraleAttribute()
    {
        return round($this->pointsMax / \Config::get('twinoid.points_max') * 100);
    }

    public function pointsMax()
    {
        return $this->achievements()
                    ->selectRaw('site_id, SUM(npoints) as max')
                    ->groupBy('site_id');
    }

    public function getPointsMaxAttribute()
    {
        // if relation is not loaded already, let's do it first
        if (!$this->relationLoaded('pointsMax')) {
            $this->load('pointsMax');
        }
        $related = $this->getRelation('pointsMax')->first();
        // then return the count directly
        return ($related) ? (int)round($related->max, 5) : 0;
    }

    public function usersCount()
    {
        return $this->users()
                    ->selectRaw('site_id, count(*) as aggregate')
                    ->groupBy('site_id');
    }

    public function getUsersCountAttribute()
    {
        // if relation is not loaded already, let's do it first
        if (!$this->relationLoaded('usersCount')) {
            $this->load('usersCount');
        }
        $related = $this->getRelation('usersCount')->first();
        // then return the count directly
        return ($related) ? (int)$related->aggregate : 0;
    }

    public function statsCount()
    {
        return $this->stats()
                    ->selectRaw('site_id, count(*) as aggregate')
                    ->groupBy('site_id');
    }

    public function getStatsCountAttribute()
    {
        // if relation is not loaded already, let's do it first
        if (!$this->relationLoaded('statsCount')) {
            $this->load('statsCount');
        }
        $related = $this->getRelation('statsCount')->first();
        // then return the count directly
        return ($related) ? (int)$related->aggregate : 0;
    }

    public function achievementsCount()
    {
        return $this->achievements()
                    ->selectRaw('site_id, count(*) as aggregate')
                    ->groupBy('site_id');
    }

    public function getAchievementsCountAttribute()
    {
        // if relation is not loaded already, let's do it first
        if (!$this->relationLoaded('achievementsCount')) {
            $this->load('achievementsCount');
        }
        $related = $this->getRelation('achievementsCount')->first();
        // then return the count directly
        return ($related) ? (int)$related->aggregate : 0;
    }
}
