<?php
namespace App\Repositories;

use App\Models\Achievement;
use App\Models\Site;
use App\Models\Stat;
use App\Models\User;
use DB;

class UserRepository extends BaseRepository
{
    public function __construct(
        User $user
    ) {
        $this->model = $user;
    }

    public static function isUserUpToDate($id, $hours)
    {
        if ($user = User::find($id)) {
            if ($user->updated_at->diffInHours(\Carbon\Carbon::now()) <= $hours) {
                return true;
            }
        }
        return false;
    }

    public function updateOrCreate($twino_user)
    {
        \Debugbar::startMeasure('updateOrCreate', 'updateOrCreate');
        if ($user = User::find($twino_user->id)) {
        } else {
            $user = new User();
        }
        $user->id     = $twino_user->id;
        $user->name   = $twino_user->name;
        $user->avatar = !empty($twino_user->picture) ? $twino_user->picture->url : null;
        $user->locale = $twino_user->locale;
        $user->touch();
        return $user;
    }

    public function updateData($user, $twino_user)
    {

        $tab_stats                  = [];
        $tab_sites                  = [];
        $tab_achievements           = [];
        $stats_by_site_and_twino_id = [];

        \Debugbar::startMeasure('get_sites', 'get sites');
        $sites = Site::get();
        \Debugbar::stopMeasure('get_sites');

        foreach ($twino_user->sites as $twino_user_site) {
            if (empty($twino_user_site->site->name) || (
                    in_array(
                        $twino_user_site->site->status,
                        ['hide', 'soon']
                    ) && $twino_user_site->site->id != 1)
            ) {
                continue;
            }
            \Debugbar::startMeasure(
                'get_site_' . $twino_user_site->site->id,
                'get_site ' . $twino_user_site->site->name
            );
            $site = $sites->find($twino_user_site->site->id);
            if (!$site) {
                $site        = new Site();
                $site->id    = $twino_user_site->site->id;
                $site->name  = $twino_user_site->site->name;
                $site->cover =
                    !empty($twino_user_site->site->infos) && !empty($twino_user_site->site->infos[0]->cover) ?
                        $twino_user_site->site->infos[0]->cover->url : null;
                $site->lang  = $twino_user_site->site->lang;
                $site->save();
            }
            \Debugbar::startMeasure('$stats_keyby');
            $stats = $site->stats->keyBy('twino_id')->all();
            \Debugbar::stopMeasure('$stats_keyby');
            if (!empty($twino_user_site->stats)) {
                foreach ($twino_user_site->stats as $twino_user_stat) {
                    \Debugbar::startMeasure('each stat ' . $twino_user_stat->id);

                    \Debugbar::startMeasure('contain stat ' . $twino_user_stat->id);
                    if (!isset($stats[$twino_user_stat->id])) {
                        $stat           = new Stat();
                        $stat->twino_id = $twino_user_stat->id;
                    } else {
                        $stat = $stats[$twino_user_stat->id];
                    }
                    \Debugbar::stopMeasure('contain stat ' . $twino_user_stat->id);

//                    $stat->translate($user->locale)->name        = $twino_user_stat->name;
//                    $stat->translate($user->locale)->description = $twino_user_stat->description;
                    if ($stat->translateOrNew($user->locale)->name != $twino_user_stat->name) {
                        $stat->translateOrNew($user->locale)->name = $twino_user_stat->name;
                    }
                    if ($stat->translateOrNew($user->locale)->description != $twino_user_stat->description) {
                        $stat->translateOrNew($user->locale)->description = $twino_user_stat->description;
                    }
                    if (!empty($twino_user_stat->icon)) {
                        $stat->icon = $twino_user_stat->icon->url;
                    }
                    $stat = $site->stats()->save($stat);

                    $stats_by_site_and_twino_id[$site->id][$stat->twino_id] = $stat;
//                    $user->stats()->sync([$stat->id => ['score' => $twino_user_stat->score]]);
//                    $tab_stats[$stat->id] = ['score' => $twino_user_stat->score];
                    $tab_stats[] = ['stat_id' => $stat->id, 'user_id' => $user->id, 'score' => $twino_user_stat->score];

                    \Debugbar::stopMeasure('each stat ' . $twino_user_stat->id);
                }
            }
            if (!empty($twino_user_site->achievements)) {
                $achievements = $site->achievements->keyBy('twino_id')->all();
                foreach ($twino_user_site->achievements as $twino_user_achievement) {
                    \Debugbar::startMeasure('each achievement ' . $twino_user_achievement->id);

                    if (!isset($achievements[$twino_user_achievement->id])) {
                        $achievement           = new Achievement();
                        $achievement->twino_id = $twino_user_achievement->id;
                        $achievement->site_id  = $site->id;
                    } else {
                        $achievement = $achievements[$twino_user_achievement->id];
                    }
                    if ($achievement->translateOrNew($user->locale)->name != $twino_user_achievement->name) {
                        $achievement->translateOrNew($user->locale)->name = $twino_user_achievement->name;
                    }
                    if (isset($twino_user_achievement->data->title) &&
                        $achievement->translateOrNew(
                            $user->locale
                        )->title != $twino_user_achievement->data->title
                    ) {
                        $achievement->translateOrNew($user->locale)->title = $twino_user_achievement->data->title;
                    }
                    $achievement->score   = $twino_user_achievement->score;
                    $achievement->name    = $twino_user_achievement->name;
                    $achievement->points  = $twino_user_achievement->points;
                    $achievement->npoints = $twino_user_achievement->npoints;
                    $achievement->type    = $twino_user_achievement->data->type;
                    $achievement->url     = isset($twino_user_achievement->data->url) ?
                        $twino_user_achievement->data->url : null;
                    $achievement->prefix  = isset($twino_user_achievement->data->prefix) ?
                        $twino_user_achievement->data->prefix : null;
                    $achievement->suffix  = isset($twino_user_achievement->data->suffix) ?
                        $twino_user_achievement->data->suffix : null;
                    if (isset($stats_by_site_and_twino_id[$site->id][$twino_user_achievement->stat])) {
                        $achievement->stat_id =
                            $stats_by_site_and_twino_id[$site->id][$twino_user_achievement->stat]->id;
                    }
                    $achievement->save();
//                    $tab_achievements[] = $achievement->id;
                    $tab_achievements[] = [
                        'achievement_id' => $achievement->id,
                        'user_id'        => $user->id,
                        'date'           => $twino_user_achievement->date
                    ];

                    \Debugbar::stopMeasure('each achievement ' . $twino_user_achievement->id);
                }
            }
            if (!array_key_exists('npoints', $twino_user_site)) {
                xdebug_break();
            }
//            $tab_sites[$site->id] = ['points' => $twino_user_site->points, 'npoints' => $twino_user_site->npoints];
            $tab_sites[] = [
                'site_id' => $site->id,
                'user_id' => $user->id,
                'points'  => $twino_user_site->points,
                'npoints' => $twino_user_site->npoints
            ];
            \Debugbar::stopMeasure('get_site_' . $twino_user_site->site->id);
        }
        /** Code simple mais fais beaucoup beaucoup beaucoup de requêtes */
//        $user->sites()->sync($tab_sites);
//        $user->stats()->sync($tab_stats);
//        $user->achievements()->sync($tab_achievements);

        //Pour économiser des requêtes
        \Debugbar::startMeasure('save user stats and achievements');
        DB::table('site_user')->where('user_id', '=', $user->id)->delete();
        DB::table('stat_user')->where('user_id', '=', $user->id)->delete();
        DB::table('achievement_user')->where('user_id', '=', $user->id)->delete();
        DB::table('site_user')->insert($tab_sites);
        DB::table('stat_user')->insert($tab_stats);
        DB::table('achievement_user')->insert($tab_achievements);
        \Debugbar::stopMeasure('save user stats and achievements');
        \Debugbar::stopMeasure('updateOrCreate');
    }
}
