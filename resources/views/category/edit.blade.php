<div class="row edit-category-modal">
    <div class="col-md-12">
        @if(isset($category))
            {{ Form::model($category, ['route' => ['category.update', $category->id], 'method' => 'patch', 'class' => 'edit-category-form']) }}
        @else
            {!! Form::open(array('route' => ['category.store'], 'method' => 'post', 'class' => 'edit-category-form')) !!}
            {!! Form::hidden('site_id', Binput::clean($site_id) ) !!}
        @endif
        <div class="form-group">
            {!! Form::label('name', 'Nom') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>
        <?php
        $categories_liste = ['' => 'Pas de parent'] + $categories->pluck('name', 'id')->toArray();
        $attrs = ['class' => 'form-control'];
        if (isset($category) && $category->children->count() > 0) {
            $attrs['disabled'] = 'disabled';
            Form::hidden('parent_id', '');
        }
        ?>
        <div class="form-group">
            {!! Form::label('parent_id', 'Catégorie parente') !!}
            {!! Form::select('parent_id', $categories_liste, isset($category->parent->id) ? $category->parent->id : '', $attrs)!!}
        </div>
        {!! Form::close() !!}
    </div>
</div>