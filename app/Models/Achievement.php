<?php

namespace App\Models;

use App\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;
use Laracasts\Presenter\PresentableTrait;

/**
 * App\Models\Achievement
 *
 * @property-read \App\Models\Stat $stat
 * @property-read \App\Models\Site $site
 * @property-read mixed $n_points
 */
class Achievement extends Model
{
    use Translatable;
    use PresentableTrait;

    const TYPE_DEFAULT = 'default';
    const TYPE_TITLE   = 'title';
    const TYPE_ICON    = 'icon';
    const TYPE_IMAGE   = 'image';

    protected $guarded = array('created_at', 'updated_at');
    protected $presenter = 'App\Presenters\AchievementPresenter';
    protected $translatedAttributes = ['name', 'title'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    public function stat()
    {
        return $this->belongsTo('App\Models\Stat');
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User');
    }

    public function site()
    {
        return $this->hasOne('App\Models\Site');
    }

    public function setNPointsAttribute($value)
    {
        $this->attributes['npoints'] = number_format($value, 13);
    }

    public function setSuffixAttribute($value)
    {
        $this->attributes['suffix'] = (string)(int)$value;
    }

    public function setPrefixAttribute($value)
    {
        $this->attributes['prefix'] = (string)(int)$value;
    }
}
